# Planning Poker #

Install [.NET Core SDK](https://www.microsoft.com/net/core#windowscmd)

Run the following commands: 

`dotnet new webapi -n SWINGLINE.Api.PokerHands`

`dotnet restore`

`dotnet run`

Use POSTMAN to send a GET request to `http://localhost:5000/api/values/`